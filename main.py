from typing import Dict, ForwardRef, List, Optional, Self, Set, Tuple
import random
import copy
import sys
import time

DEBUG = True


def err(msg: str):
    print("[ERROR]: {}".format(msg))
    sys.exit(1)


def dbg(msg: str):
    global DEBUG
    if DEBUG:
        print("[DEBUG]: {}".format(msg))


class Vertex(object):
    def __init__(self, id: int):
        self.id: int = id
        self.edges: List[Vertex] = []
        self.candidate_color: Optional[int] = None
        self.final_color: Optional[int] = None
        self.neighbor_candidates: Set[int] = set()

    def connect_with(self, neighbor: Self):
        """ Creates underacted graph connection between two nodes"""
        if neighbor not in self.edges:
            self.edges.append(neighbor)
        if self not in neighbor.edges:
            neighbor.edges.append(self)

    def neighbor_final_colors(self) -> Set[int]:
        ret = set()
        for neighbor in self.edges:
            if neighbor.final_color is not None:
                ret.add(neighbor.final_color)
        return ret

    def gen_candidate_color(self, delta: Set[int]):
        nc: Set[int] = self.neighbor_final_colors()
        candidate_colors_set = delta.difference(nc)
        self.candidate_color = random.choice(list(candidate_colors_set))
        self.neighbor_candidates = set()
        dbg(f"v: {self.id} gen_cc: {self.candidate_color}, delta:{delta}")

    def receive_neighbor_candidate(self, neightbor_candidate_color: int):
        self.neighbor_candidates.add(neightbor_candidate_color)

    def collect_candidates(self):
        self.neighbor_candidates = set()
        for n in self.edges:
            nc = n.candidate_color
            if nc is not None:
                dbg(f"vertex {self.id} collect neighbor {n.id} as {nc}")
                self.neighbor_candidates.add(nc)

    def apply_candidate_color(self) -> Optional[int]:
        if self.candidate_color is None:
            dbg("Tried to apply candidate color without having one.\n"
                "id:{} candidate_color:{} edges:{}".format(self.id, self.candidate_color, self.edges))
        else:
            if self.candidate_color not in self.neighbor_candidates:
                self.final_color = self.candidate_color
                dbg("Vetex: {} finalized to color: {}".format(
                    self.id, self.final_color))
                self.candidate_color = None
                return self.id
            else:
                dbg("Vertex: {} cound not finalize to: {}, neighbor candidates:{}".format(
                    self.id, self.candidate_color, self.neighbor_candidates))
                return None


class Graph(object):
    def __init__(self):
        super(Graph, self).__init__()
        self.uncolored: Dict[int, Vertex] = {}
        self.colored: Dict[int, Vertex] = {}
        self.color_set: Set[int] = set()

    def add_vertex(self, vertex: Vertex):
        if vertex.id not in self.uncolored:
            self.uncolored[vertex.id] = vertex

    def new_edge(self, a: Vertex, b: Vertex):
        self.add_vertex(a)
        self.add_vertex(b)
        a.connect_with(b)

    def max_deg(self) -> int:
        deg: int = 0
        vertex: Vertex
        for _, vertex in self.uncolored.items():
            deg = max(deg, len(vertex.edges))
        dbg(f"Graph max degree: {deg}")
        return deg

    def color(self):
        self.color_set = set(range(self.max_deg() + 1))

        # iterate as long as we have uncolored vertices and hope our algorithm terminates
        while len(self.uncolored) > 0:
            dbg(f"Coloring vertices {self.uncolored.keys()}")
            for _, uc in self.uncolored.items():
                # generate new candidate color for each uncolored vertex
                uc.gen_candidate_color(self.color_set)
            for _, uc in self.uncolored.items():
                # broadcast candidate color to neighbors
                uc.collect_candidates()
            final_color_ids: List[int] = []
            for _, uc in self.uncolored.items():
                # apply candidate color if available
                ret: Optional[int] = uc.apply_candidate_color()
                uc.candidate_color = None
                uc.neighbor_candidates = set()
                if ret is not None:
                    final_color_ids.append(ret)
            # move newly colored vertices to finally colored vertices
            for final_id in final_color_ids:
                self.colored[final_id] = self.uncolored[final_id]
                self.uncolored.pop(final_id)


class Bench:
    """Benchmark"""

    def __init__(self, vertices: int, edges: int):
        dbg(f"New benchmark: {vertices} vertices and {edges} edges")
        rv = range(vertices)
        re: set[tuple[int, int]] = set()
        re_range = list(range(vertices))
        self.graph = Graph()
        self.v: dict[int, Vertex] = {}
        for id in rv:
            self.v[id] = Vertex(id)
            self.graph.add_vertex(self.v[id])
        dbg(f"Generated {vertices} vertices")
        if vertices < 10:
            for id, _ in self.v.items():
                dbg(f"{id}")
        while len(re) <= edges:
            ne = (random.choice(re_range), random.choice(re_range))
            dbg(f"new edge candidate: {ne}")
            if ne[0] == ne[1]:
                # no self looping edges
                continue
            if ne not in re and (ne[1], ne[0]) not in re:
                # only vundirected edges
                re.add(ne)
        if edges < 10:
            for e_start, e_end in re:
                dbg(f"{e_start} <--> {e_end}")
        dbg(f"Generated {edges} edges")
        for e in re:
            self.graph.new_edge(self.v[e[0]], self.v[e[1]])
        dbg("Starting Coloring")
        start = time.time()
        # sys.exit(1)
        self.graph.color()
        dbg(f"Coloring took: {time.time() - start} seconds")


def main():
    # b = Bench(5, 6)
    b = Bench(1000, 10000)
    print("Hello.")


if __name__ == '__main__':
    main()
